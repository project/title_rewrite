<?php

/**
 * @file
 * The form logic for the title rewrite module.
 */


/**
 * Build the settings form for the module.
 *
 * @return
 *  The form array.
 */
function title_rewrite_admin_overview() {
  $form = array();

  $result = db_query('SELECT * from {title_rewrite} ORDER BY weight ASC');
  $titles = array();
  while ($row = db_fetch_array($result)) {
    $id = $row['id'];
    
    $form[$id]['id'] = array(
      '#type' => 'value',
      '#value' => $id,
    );
    
    $form[$id]['name'] = array('#value' => $row['name']);
    $form[$id]['weight'] = array('#value' => $row['weight']);
    $form[$id]['edit'] = array('#value' => l(t('Edit'), 'admin/settings/custom_title/edit/'. $id));
    $form[$id]['delete'] = array('#value' => $default ? '' : l(t('Delete'), 'admin/settings/custom_title/delete/'. $id));
  }
  
  return $form;
}


/**
 * Generate the token reference.
 *
 * @return
 *  The form data to display.
 */
function title_rewrite_tokenref_form() {
  $form = array();
  
  $tokens = token_get_list();
  
  // -------------------------------------------------------------------------
  // The global tokens section
  $form['global_tokens'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Global Tokens'),
    '#collapsible' => TRUE,
    '#weight'      => -5,
    '#description' => t('Global tokens are available site wide. They may be used for title rewrite rules that affect any page.'),
  );
  
  foreach($tokens['global'] as $token => $description) {
    $form['global_tokens'][$token]['left'] = array(
      '#type'  => 'item',
      '#value' => '[' . $token . ']',
    );
    
    $form['global_tokens'][$token]['right'] = array(
      '#type'  => 'item',
      '#value' => $description,
    );
  }
  
  // -------------------------------------------------------------------------
  // The node tokens section
  $form['node_tokens'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Node Tokens'),
    '#collapsible' => TRUE,
    '#weight'      => -3,
    '#description' => t('Node tokens are available for page-level node presentation.'),
  );
  
  foreach($tokens['node'] as $token => $description) {
    $form['node_tokens'][$token]['left'] = array(
      '#type'  => 'item',
      '#value' => '[' . $token . ']',
    );
    
    $form['node_tokens'][$token]['right'] = array(
      '#type'  => 'item',
      '#value' => $description,
    );
  }
  
  return $form;
}

/**
 * Generate the form for creating a new custom title rule or editing an
 * existing one.
 *
 * @param $id
 *  The ID of an existing title pattern to edit. If specified, this title pattern
 *  will be used to populate the form fields.
 *
 * @return
 *  The form contents.
 */
function title_rewrite_create_form($id = 0) {
  $form = array();
  
  $existing = ($id > 0) ? _title_rewrite_load_instance($id) : NULL;
  
  $form['rewrite_rule'] = array(
    '#type'        => 'fieldset',
    '#title'       => ($existing !== NULL) ? t('Edit Existing Title Rewrite Rule')
                                           : t('Create New Title Rewrite Rule'),
    '#collapsible' => FALSE,
    '#weight'      => -5,
  );
  
  $form['rewrite_rule']['pattern_id'] = array(
    '#type'  => 'value',
    '#value' => ($existing !== NULL) ? $existing['id'] : -1,
  );
  
  $form['rewrite_rule']['name'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Name'),
    '#default_value' => ($existing !== NULL) ? $existing['name'] : '',
    '#description'   => t('A unique name to help you remember what this rewrite rule does when it\'s shown in the list view.'),
  );
  
  $weights = 0;
  
  $form['rewrite_rule']['weight'] = array(
    '#type'        => 'select',
    '#title'       => t('Weight'),
    '#default_value' => ($existing !== NULL) ? $existing['weight'] : 0,
    '#options'     => drupal_map_assoc(range(-10, 10)),
    '#description' => t('This determines the order that rules are evaluated. Rules with lower weight values are evaluated before rules with higher weights.'),
  );
  
  $description = t('This text will be used to replace the contents of the HTML %title element on pages matching the paths below. You may include any of the tokens specified on the !ref_page in this field.',
                   array(
                    '%title'    => '<title>',
                    '!ref_page' => l(t('Token Reference Page'), 'admin/settings/custom_title/tokens')
                   )
                  );
  
  $form['rewrite_rule']['title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Rewrite Text'),
    '#default_value' => ($existing !== NULL) ? $existing['token_title'] : '',
    '#description'   => $description,
  );

  $options = array(
    1 => t('Apply rewrite only the listed pages.'),
    2 => t('Show if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).'),
  );

  $form['rewrite_rule']['type'] = array(
    '#type'        => 'radios',
    '#title'       => t('Rule Application'),
    '#default_value' => ($existing !== NULL) ? $existing['type'] : 1,
    '#options'     => $options,
    '#description' => t('Determines the method used apply the re-write rule.'),
  );

  $description = t('Enter one page per line as Drupal paths. The "*" character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page. If the PHP-mode is chosen, enter PHP code between %php. Note that executing incorrect PHP-code can break your Drupal site.',
                   array('%blog'          => 'blog', 
                         '%blog-wildcard' => 'blog/*', 
                         '%front'         => '<front>',
                         '%php'           => '<?php ?>')
                  );
  
  $form['rewrite_rule']['paths'] = array(
      '#type'          => 'textarea',
      '#title'         => t('Path(s)'),
      '#default_value' => ($existing !== NULL) ? $existing['path_expr'] : '',
      '#description'   => $description,
    );
    
  $form['submit'] = array(
    '#type'  => 'submit', 
    '#value' => t('Save Rewrite Rule'),
  );
  
  $form['cancel'] = array(
    '#type'  => 'submit',
    '#value' => t('Cancel'),
  );
  
  return $form;
}


/**
 * Validate the pattern editing/creation form.
 *
 * @param $form_id
 *  The ID of the form.
 * @param $form_values
 *  The form values.
 */
function title_rewrite_create_form_validate($form_id, $form_values) {
  // Make sure that the name is unique.
  $name = trim($form_values['name']);
  $pid  = $form_values['pattern_id'];

  $count = db_num_rows(db_query('SELECT name FROM {title_rewrite} WHERE id != %d AND name = \'%s\'', $pid, $name));
  
  if ($count > 0) {
    form_set_error('name', t('Please ensure that each pattern name is unique.'));
  }
}


/**
 * Save the data from the pattern editing/creation form.
 *
 * @param $form_id
 *  The ID of the form.
 * @param $form_values
 *  The form values. 
 */
function title_rewrite_create_form_submit($form_id, $form_values) {
  $pid         = $form_values['pattern_id'];
  $name        = trim($form_values['name']);
  $token_title = trim($form_values['title']);
  $path_expr   = trim($form_values['paths']);
  $weight      = intval($form_values['weight']);
  $type        = intval($form_values['type']);
  
  // -------------------------------------------------------------------------
  // Bail out on cancel.
  if ($form_values['op'] == t('Cancel')) {
    return 'admin/settings/custom_title';
  }
  
  // -------------------------------------------------------------------------
  // Existing (if) or new (else)?
  if ($pid > -1) {
    db_query('UPDATE {title_rewrite} SET name = \'%s\', path_expr = \'%s\', 
                                    token_title = \'%s\', weight = %d, type = %d 
                     WHERE id = %d',
             $name, $path_expr, $token_title, $weight, $type, $pid);
    drupal_set_message(t('Updated title pattern \'@name\'', array('@name' => $name)));
  }
  else {
    db_query('INSERT INTO {title_rewrite} (name, path_expr, token_title, weight, type)
                     VALUES (\'%s\', \'%s\', \'%s\', %d, %d)',
             $name, $path_expr, $token_title, $weight, $type);
    drupal_set_message(t('Created custom title pattern \'@name\'', array('@name' => $name)));
  }
  
  return 'admin/settings/custom_title';
}


/**
 * Generate the confirmation form for deleting a title.
 *
 * @param $id
 *  The ID of the form to delete.
 *
 * @return
 *  The form elements.
 */
function title_rewrite_delete_form($id) {
  $form = array();
  
  $existing = _title_rewrite_load_instance($id);
  
  $form['pattern_id'] = array(
    '#type'  => 'value',
    '#value' => $id,
  );

  $question = '<p>' . 
              t('You are about to remove the title re-write rule "%name".',
                array('%name' => $existing['name']));
  $desc     = '<p>' . t('This rule matches paths using the pattern !pat and creates a title using the token pattern !token',
                array('!pat' => '"<em>' . $existing['path_expr'] . '</em>"', 
                      '!token' => '"<em>' . $existing['token_title'] . '</em>"')) .
              '</p><p>' . t('This action cannot be undone.') . '</p>';

  return confirm_form($form, $question, 'admin/settings/custom_title', $desc, t('Delete'), t('Cancel'));
}


/**
 * Handle the delete form submission.
 *
 * @param $form_id
 *  The form's ID
 * @param $form_values
 *  The form values.
 *
 * @return
 *  The path to send the browser to after the form is submitted.
 */
function title_rewrite_delete_form_submit($form_id, $form_values) {
  $id = $form_values['pattern_id'];
  
  db_query('DELETE FROM {title_rewrite} WHERE id = %d', $id);
  drupal_set_message("Custom title rule deleted.");
  
  return 'admin/settings/custom_title';
}


/**
 * Theme the table in the admin overview form.
 *
 * @param $form
 *  The form elements to theme.
 *
 * @return
 *  The rendered form elements.
 */
function theme_title_rewrite_admin_overview($form) {
  $rows = array();
  foreach ($form as $name => $element) {
    if (isset($element['name']) && is_array($element['name'])) {
      $rows[] = array(
        drupal_render($element['name']),
        drupal_render($element['weight']),
        drupal_render($element['edit']),
        drupal_render($element['delete'])
      );
      unset($form[$name]);
    }
  }
  $header = array(t('Name'), t('Weight'), t('Edit'), t('Delete'));
  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);

  return $output;
}


/**
 * Theme the token reference form.
 *
 * @param $form
 *  The token reference form.
 *
 * @return
 *  The themed form.
 */
function theme_title_rewrite_tokenref_form($form) {
  // -------------------------------------------------------------------------
  // The global tokens.
  $rows = array();
  foreach ($form['global_tokens'] as $name => $element) {
    if (isset($element['left']) && is_array($element['left'])) {
      $rows[] = array(
        array(
          'data'  => drupal_render($element['left']), 
          'class' => 'title-rewrite-ref-left'
        ),
        array(
          'data' => drupal_render($element['right']),
          'class' => 'title-rewrite-ref-right'
        ),
      );
      
      unset($form['global_tokens'][$name]);
    }
  }

  $form['global_tokens']['tokens'] = array(
    '#value' => theme('table', array(), $rows, array('class' => 'title-rewrite-token-table')),
  );
  
  // -------------------------------------------------------------------------
  // The node tokens.
  $rows = array();
  foreach ($form['node_tokens'] as $name => $element) {
    if (isset($element['left']) && is_array($element['left'])) {
      $rows[] = array(
        array(
          'data'  => drupal_render($element['left']), 
          'class' => 'title-rewrite-ref-left'
        ),
        array(
          'data' => drupal_render($element['right']),
          'class' => 'title-rewrite-ref-right'
        ),
      );
      
      unset($form['node_tokens'][$name]);
    }
  }

  $form['node_tokens']['tokens'] = array(
    '#value' => theme('table', array(), $rows, array('class' => 'title-rewrite-token-table')),
  );

  return drupal_render($form);
}

