<?php

/**
 * @file
 * Provides a system for creating custom content for the title element of 
 * HTML pages.
 */

/** The permission for administering custom titles. */
define('TITLE_REWRITE_PERM_ADMIN', 'administer custom titles');

require_once(drupal_get_path('module', 'title_rewrite') . '/title_rewrite_forms.inc');


/**
 * Implementation of hook_help().
 *
 * TODO - Add documentation for 'admin/help#custom_title' and
 *        'admin/help/custom_title'
 */
function title_rewrite_help($section) {
  switch($section) {
    case 'admin/settings/custom_title':
      $help = '<p>' . t('Title rewrite rules allow the contents of the XHTML &lt;title&gt; element to be overridden using text specified by an administrator. The override text may include dynamic content using the tokens specified in the !ref_link page.',
                array('!ref_link' => l(t('Token Reference Page'), 'admin/settings/custom_title/tokens'))
              ) . '</p>';
      $help .= '<p>' . t('Rules are evaluated by their weight. Lighter rules are evaluated before heavier ones. Rule evaluation stops on the first rule encountered that matches any given path. For example, if the lightest weight rule matches a path, that rule is evaluated and no further rules are evaluated against that path.') . '</p>';
      return $help;
    case 'admin/settings/custom_title/new':
      $help = '<p>' . t('When using tokens in the Rewrite Text field, keep in mind that node tokens are only available when a page is displaying a single node. Global tokens are available on all pages.') . '</p>';
      return $help;   
  }
}


/**
 * Implementation of hook_menu().
 */
function title_rewrite_menu($may_cache) {
  $items = array();
  
  if (!$may_cache) {
    $items[] = array(
      'path'     => 'admin/settings/custom_title',
      'title'    => t('Title Rewrite Rules'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('title_rewrite_admin_overview'),
      'type'     => MENU_NORMAL_ITEM,
      'access'   => user_access(TITLE_REWRITE_PERM_ADMIN),
    );
    
    $items[] = array(
      'path'     => 'admin/settings/custom_title/list',
      'title'    => t('List'),
      'callback' => 'title_rewrite_admin_overview',
      'type'     => MENU_DEFAULT_LOCAL_TASK,
      'access'   => user_access(TITLE_REWRITE_PERM_ADMIN),
    );
    
    $items[] = array(
      'path'     => 'admin/settings/custom_title/new',
      'title'    => t('New Rewrite Rule'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('title_rewrite_create_form'),
      'type'     => MENU_LOCAL_TASK,
      'weight'   => 1,
      'access'   => user_access(TITLE_REWRITE_PERM_ADMIN),
    );
    
    $items[] = array(
      'path'     => 'admin/settings/custom_title/tokens',
      'title'    => t('Token Reference'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('title_rewrite_tokenref_form'),
      'type'     => MENU_LOCAL_TASK,
      'weight'   => 2,
      'access'   => user_access(TITLE_REWRITE_PERM_ADMIN),
    );
    
    $items[] = array(
      'path' => 'admin/settings/custom_title/edit/' . arg(4),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('title_rewrite_create_form', arg(4)),
      'type'     => MENU_CALLBACK,
      'access'   => user_access(TITLE_REWRITE_PERM_ADMIN),
    );
    
    $items[] = array(
      'path' => 'admin/settings/custom_title/delete/' . arg(4),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('title_rewrite_delete_form', arg(4)),
      'type'     => MENU_CALLBACK,
      'access'   => user_access(TITLE_REWRITE_PERM_ADMIN),
    );
  }
  
  return $items;
}


/**
 * Implementation of hook_perm().
 */
function title_rewrite_perm() {
  return array(TITLE_REWRITE_PERM_ADMIN);
}


/**
 * Load a custom title by its ID.
 *
 * @param $id
 *  The unique ID of the custom title to load.
 *
 * @return
 *  An array containing the custom title information, or NULL if no title
 *  with the given ID was found.
 */
function _title_rewrite_load_instance($id) {
  $title  = NULL;
  $result = db_query('SELECT id, name, type, path_expr, token_title, weight 
                             FROM {title_rewrite} WHERE id = %d', $id);
  
  if (db_num_rows($result) > 0) {
    $title = db_fetch_array($result);
  }

  return $title;
}


/**
 * Retrieve all the custom title patterns.
 *
 * @return
 *  An array of associative arrays with they keys 'path_expr' and 
 *  'toke_title'.
 */
function _title_rewrite_get_all() {
  $paths  = array();
  $result = db_query('SELECT path_expr, token_title, type FROM {title_rewrite} ORDER BY weight ASC');
  
  while ($row = db_fetch_array($result)) {
    $paths[] = $row;
  }
  
  return $paths;
}


/**
 * Retrieve the title rewrite rule for the given path.
 *
 * @param $path
 *  The Drupal path to replace the title for.
 *
 * @return
 *  The title rewrite rule.
 */
function _title_rewrite_get_rule($path) {
  $rules = _title_rewrite_get_all();
  
  foreach($rules as $rule) {
    if ($rule['type'] == 1) {
      $regexp = '/^('. 
                preg_replace(array('/(\r\n?|\n)/', '/\\\\\*/', '/(^|\|)\\\\<front\\\\>($|\|)/'), 
                             array('|', '.*', '\1'. preg_quote(variable_get('site_frontpage', 'node'), '/') .'\2'), 
                             preg_quote($rule['path_expr'], '/')) .
                ')$/';
                
      // Compare with the Drupal path and the query.
      $page_match = preg_match($regexp, $path);
      if ($path != $_GET['q']) {
        $page_match = $page_match || preg_match($regexp, $_GET['q']);
      }
    }
    elseif ($rule['type'] == 2) {
      $page_match = drupal_eval($rule['path_expr']);
    }

    if ($page_match > 0) {
      return $rule;
    }
  }

  return NULL;
}


/**
 * Evaluate the title rewrite rules agains the current page and generate a
 * re-written title if a rule is available.
 *
 * @param $current_title
 *  The title that would be displayed on the page if no re-write rule was 
 *  present. If no rule matches this page's path, this value will be
 *  returned.
 * @param &$node
 *  If this page is a node page, template.php should pass the node. This
 *  will enable the re-write rule to make use of node tokens.
 *
 * @return
 *  The rewritten title for the current page. If no rule was available
 *  for this page, the value passed to $current_title will be returned.
 */
function title_rewrite_page_get_title($current_title = NULL, &$node = NULL) {
  $path = drupal_get_path_alias($_GET['q']);
  $rule = _title_rewrite_get_rule($path);
  $result = $current_title;
  
  $scope = ($node != NULL) ? 'node' : 'global';
  
  if ($rule !== NULL) {
    $result = token_replace($rule['token_title'], $scope, $node);
  }
  
  return $result;
}
