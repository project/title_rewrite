<?php

function _phptemplate_variables($hook, $vars = array()) {

  if ($hook == 'page') {
    if (module_exists('title_rewrite')) {
      $current = isset($vars['head_title']) ? $vars['head_title'] : NULL;
      $vars['head_title'] = title_rewrite_page_get_title($current, $vars['node']);
    }
  }
  
  return $vars;
}
